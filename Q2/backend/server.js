const express = require("express");
const app = express();
const db = require("./db");
const utils = require("./utils");

app.use(express.json());

app.get("/", (request,response) => {
    const {movie_title}= request.body;
    const statement= `select * from Movie where movie_title=${movie_title}`;
    const connection= db.openConnection();
    connection.query(statement,(error,data) => {
        response.send(utils.createResult(error,data));
    });
});

app.post("/", (request,response) => {
    const {movie_title, movie_release_date,movie_time,director_name}= request.body;
    const statement= `insert into Movie(movie_title, movie_release_date,movie_time,director_name) values('${movie_title}','${movie_release_date}','${movie_time}','${director_name}')`;
    const connection= db.openConnection();
    connection.query(statement,(error,data) => {
        response.send(utils.createResult(error,data));
    });
});

app.put("/:id", (request,response) => {
    const {movie_release_date,movie_time}= request.body;
    const {id}= request.params;
    const statement= `update Movie set movie_release_date=${movie_release_date} and movie_time=${movie_time} where movie_id=${id} `;
    const connection= db.openConnection();
    connection.query(statement,(error,data) => {
        response.send(utils.createResult(error,data));
    });
});

app.delete("/:id", (request,response) => {
    const {id}= request.params;
    const statement= `delete from Movie where movie_id=${id}`;
    const connection= db.openConnection();
    connection.query(statement,(error,data) => {
        response.send(utils.createResult(error,data));
    });
});

app.listen(4000, () => {
    console.log("Server started on port 4000");
});

