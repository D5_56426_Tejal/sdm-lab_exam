ALTER USER 'root' IDENTIFIED WITH mysql_native_password BY 'root';

create table Movie(
    movie_id integer primary key auto_increment,
    movie_title varchar(100),
    movie_release_date varchar(11),
    movie_time varchar(100),
    director_name varchar(100)
    );